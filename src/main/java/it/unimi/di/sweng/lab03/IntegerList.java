package it.unimi.di.sweng.lab03;

import java.util.Scanner;

public class IntegerList {
	/*Fields*/
	IntegerNode head = null;
	
	/*Constructor*/
	public IntegerList(){		
	}
	
	public IntegerList(String s){
		Scanner scan = new Scanner(s);
		while (scan.hasNext()){
			addLast(Integer.valueOf(scan.next()));
		}
		scan.close();
		
	}
	
	/*Methods*/
	public String toString(){
		StringBuilder stringa = new StringBuilder("[");
		IntegerNode current = head;
		while(current != null){
			stringa.append(current.value() + " ");
			current = current.next();
		}
		stringa.append("]");
		return stringa.toString();
	}

	public void addLast(int i) {
		if(head == null)
			head = new IntegerNode(i);
		else{
			IntegerNode current = head;
			while(current.next()!=null){
				current = current.next();
			}
			current.addNext(new IntegerNode(i));
		}
		
	}
	
}
