package it.unimi.di.sweng.lab03;

public class IntegerNode {
	/*Fields*/
	private int val;
	private IntegerNode next = null;
	/*Constructor*/
	
	public IntegerNode(int n){
		val = n;
	}
	
	/*Methods*/
	
	public IntegerNode next(){
		return next;
	}
	
	public int value(){
		return val;
	}
	
	public void addNext(IntegerNode i){
		next = i;
	}
}
