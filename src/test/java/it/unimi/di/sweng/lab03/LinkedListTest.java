package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Test
 	public void firstTestToBeReplaced() {
 		list = new IntegerList();
 		assertThat(list.toString()).isEqualTo("[]");
 	}
	
 	@Test
 	public void addLast(){
 		list = new IntegerList();
 		list.addLast(1);
 		list.addLast(3);
 		assertThat(list.toString()).isEqualTo("[1 3 ]");
 	}
	
	@Test
 	public void costruttoreString(){
 		list = new IntegerList("1 2 3");
 		assertThat(list.toString()).isEqualTo("[1 2 3 ]");
 	}
	
	@Test (expected = IllegalArgumentException.class)
 	public void robustezzaCostruttore(){
 		list = new IntegerList("1 2 aaaa");
 	}
}
